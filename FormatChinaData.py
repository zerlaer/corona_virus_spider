import re
import pandas as pd
from win10toast import ToastNotifier

excel = pd.read_excel(r'\\10.28.0.115\share\Document\corona_virus\最近一日中国各省疫情数据.xlsx')
df = pd.DataFrame(excel)
notice = ToastNotifier()
df.drop(df.columns[[0, 7, 8, 9, 10, 11, 12, 13, 14, 15]], axis=1, inplace=True)
da = df.rename(columns={'provinceShortName': '地区', 'currentConfirmedCount': '现存确诊', 'confirmedCount': '累计确诊',
                        'suspectedCount': '疑似病例', 'curedCount': '治愈', 'deadCount': '死亡人数'})
da.to_excel(r'\\10.28.0.115\share\Document\corona_virus\整理后的全国最新疫情数据.xlsx', index=None)
