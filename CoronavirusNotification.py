# -*- coding: utf-8 -*-
# @Time    : 2021/7/19 15:22
# @FileName: corona_virus_notice.py
# @Software: PyCharm
# @Author  : Zerlaer
# @Blog    : https://zerlaer.com/
import json
import os
import re

import pandas as pd
import requests

from bs4 import BeautifulSoup
import xlwings as xw


# 采集最近一日中国各省疫情数据


class CoronaVirusSpider(object):

    def __init__(self):
        self.home_url = 'https://ncov.dxy.cn/ncovh5/view/pneumonia'

    def get_content_from_url(self, url):
        """
        根据url,获取响应内容的字符串数据
        :param url :请求url
        :return: 响应内容的字符串
        """
        response = requests.get(url)
        return response.content.decode()

    def parse_home_page(self, home_page, tag):
        """
        解析首页内容，获取Python数据
        :param home_page:首页内容
        :return: 解析后的Python数据
        """
        soup = BeautifulSoup(home_page, 'lxml')
        script = soup.find(id=tag)
        text = script.string
        json_str = re.findall(r'\[.+]', text)[0]
        data = json.loads(json_str)
        return data

    def save_to_json(self, data, path):

        with open(path, 'w', encoding='utf-8') as fp:
            json.dump(data, fp, ensure_ascii=False)

    def save_to_excel(self, json_file, excel_file):
        df = pd.read_json(json_file, encoding='UTF-8')  # 如果文本里有汉字格式，此处需要设置encoding= 'UTF-8'，否则汉字会乱码
        df.to_excel(excel_file, index=False)  # 不要索引

    def last_day_corona_virus_of_china(self):
        """
        采集最近一日中国各省疫情数据
        :return:
        """
        # 1.发送请求，获取疫情数据
        home_page = self.get_content_from_url(self.home_url)
        # 2.解析首页内容，获取最近一日中国各省疫情数据
        last_day_china_list = self.parse_home_page(home_page, 'getAreaStat')
        # 3.保存数据
        json_file = '最近一日中国各省疫情数据.json'
        excel_file = '最近一日中国各省疫情数据.xlsx'
        self.save_to_json(last_day_china_list, json_file)
        self.save_to_excel(json_file, excel_file)

    def corona_virus_excel(self):
        excel = pd.read_excel('最近一日中国各省疫情数据.xlsx')
        df = pd.DataFrame(excel)
        dangerAreas = df['dangerAreas']
        areas = dangerAreas.tolist()
        danger_list = [i for i in areas if i != '[]']
        city_list = []
        area_list = []
        for text in danger_list:
            result = re.findall(r'[\u4e00-\u9fa5]+', text)
            for city, area in zip(result[::2], result[1::2]):
                city_list.append(city)
                area_list.append(area)

        wb = xw.Book()
        wb.save('最新疫情高风险地区.xlsx')
        wb.close()
        app = xw.App(visible=False, add_book=True)
        app.display_alerts = False
        app.screen_updating = False
        wb = app.books.open('最新疫情高风险地区.xlsx')
        sht = wb.sheets('Sheet1')
        title = ['城市名称', '地区名称']
        sht.range("A1").expand('table').value = title
        info = sht.used_range
        nrows = info.last_cell.row
        sht.range("A%s" % str(nrows + 1)).options(transpose=True).value = city_list
        sht.range("B%s" % str(nrows + 1)).options(transpose=True).value = area_list
        wb.save()
        wb.app.quit()
        os.remove('最近一日中国各省疫情数据.json')
        os.remove('最近一日中国各省疫情数据.xlsx')

    def run(self):
        self.last_day_corona_virus_of_china()
        self.corona_virus_excel()


if __name__ == '__main__':
    spider = CoronaVirusSpider()
    spider.run()
