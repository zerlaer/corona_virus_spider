# COVID-19 疫情动态爬虫



> 数据来源：[丁香园](https://ncov.dxy.cn/ncovh5/view/pneumonia)

- LastDayByCountry.py  采集最近一日各国疫情数据
- LastDayByChina.py 采集最近一日中国各省疫情数据
- FromJanuaryByCountry.py 采集1月23日至今各国疫情数据
- FromJanuaryByChina.py 采集1月23日至今中国疫情数据

